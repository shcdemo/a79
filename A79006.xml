<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A79006">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>By the King. A proclamation for the free and safe passage of all clothes, goods, wares, and merchandize to our city of London.</title>
    <author>England and Wales. Sovereign (1625-1649 : Charles I)</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A79006 of text R211521 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Thomason 669.f.5[114]). Textual changes  and metadata enrichments aim at making the text more  computationally tractable, easier to read, and suitable for network-based collaborative curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in a standardized format that preserves archaic forms ('loveth', 'seekest'). Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 3 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2017</date>
    <idno type="DLPS">A79006</idno>
    <idno type="STC">Wing C2613</idno>
    <idno type="STC">Thomason 669.f.5[114]</idno>
    <idno type="STC">ESTC R211521</idno>
    <idno type="EEBO-CITATION">99870238</idno>
    <idno type="PROQUEST">99870238</idno>
    <idno type="VID">160826</idno>
    <idno type="PROQUESTGOID">2248506619</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A79006)</note>
    <note>Transcribed from: (Early English Books Online ; image set 160826)</note>
    <note>Images scanned from microfilm: (Thomason Tracts ; 245:669f5[114])</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>By the King. A proclamation for the free and safe passage of all clothes, goods, wares, and merchandize to our city of London.</title>
      <author>England and Wales. Sovereign (1625-1649 : Charles I)</author>
      <author>Charles I, King of England, 1600-1649.</author>
     </titleStmt>
     <extent>1 sheet ([1] p.)</extent>
     <publicationStmt>
      <publisher>by Alice Norton,</publisher>
      <pubPlace>[London :</pubPlace>
      <date>1642]</date>
     </publicationStmt>
     <notesStmt>
      <note>Imprint from Wing.</note>
      <note>With engraving of royal seal at head of document, between two bands of ornament and initials C. R.</note>
      <note>"Given at Our court at Oxford, the eight day of December, in the eighteenth yeare of Our reigne. God save the King."</note>
      <note>Reproduction of the original in the British Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>London (England) -- History -- 17th century -- Early works to 1800.</term>
     <term>Great Britain -- History -- Civil War, 1642-1649 -- Early works to 1800.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:tcp>A79006</ep:tcp>
    <ep:estc> R211521</ep:estc>
    <ep:stc> (Thomason 669.f.5[114]). </ep:stc>
    <ep:corpus>civilwar</ep:corpus>
    <ep:workpart>no</ep:workpart>
    <ep:title>By the King. A proclamation for the free and safe passage of all clothes, goods, wares, and merchandize to our city of London.</ep:title>
    <ep:author>England and Wales. Sovereign</ep:author>
    <ep:publicationYear>1642</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>433</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change><date>2008-03</date><label>TCP</label>
        Assigned for keying and markup
      </change>
   <change><date>2008-06</date><label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
   <change><date>2008-07</date><label>Paul Schaffner</label>
        Sampled and proofread
      </change>
   <change><date>2008-07</date><label>Paul Schaffner</label>
        Text and markup reviewed and edited
      </change>
   <change><date>2008-09</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A79006-e10">
  <body xml:id="A79006-e20">
   <pb facs="tcp:160826:1" rend="simple:additions" xml:id="A79006-001-a"/>
   <div type="Royal_proclamation" xml:id="A79006-e30">
    <opener xml:id="A79006-e40">
     <signed xml:id="A79006-e50">
      <figure xml:id="A79006-e60">
       <figDesc xml:id="A79006-e70">royal blazon or coat of arms</figDesc>
       <head xml:id="A79006-e80">
        <w lemma="c" pos="sy" xml:id="A79006-001-a-0010">C</w>
        <w lemma="r" pos="sy" xml:id="A79006-001-a-0020">R</w>
       </head>
       <p xml:id="A79006-e90">
        <w lemma="honi" pos="ffr" xml:id="A79006-001-a-0030">HONI</w>
        <w lemma="soit" pos="ffr" xml:id="A79006-001-a-0040">SOIT</w>
        <w lemma="qvi" pos="ffr" reg="x" xml:id="A79006-001-a-0050">QVI</w>
        <w lemma="mal" pos="ffr" xml:id="A79006-001-a-0060">MAL</w>
        <w lemma="y" pos="ffr" xml:id="A79006-001-a-0070">Y</w>
        <w lemma="PENSE" pos="ffr" xml:id="A79006-001-a-0080">PENSE</w>
       </p>
       <p xml:id="A79006-e100">
        <w lemma="dieu" pos="ffr" reg="Dieu" xml:id="A79006-001-a-0090">DIEV</w>
        <w lemma="et" pos="ffr" xml:id="A79006-001-a-0100">ET</w>
        <w lemma="mon" pos="ffr" xml:id="A79006-001-a-0110">MON</w>
        <w lemma="droit" pos="ffr" xml:id="A79006-001-a-0120">DROIT</w>
       </p>
      </figure>
     </signed>
     <signed xml:id="A79006-e110">
      <w lemma="c" pos="sy" xml:id="A79006-001-a-0130">C</w>
      <w lemma="R" pos="sy" xml:id="A79006-001-a-0140">R</w>
      <lb xml:id="A79006-e120"/>
      <w lemma="by" pos="acp" xml:id="A79006-001-a-0150">BY</w>
      <w lemma="the" pos="d" xml:id="A79006-001-a-0160">THE</w>
      <w lemma="king" pos="n1" xml:id="A79006-001-a-0170">KING</w>
      <pc unit="sentence" xml:id="A79006-001-a-0180">.</pc>
     </signed>
    </opener>
    <head xml:id="A79006-e130">
     <hi xml:id="A79006-e140">
      <w lemma="a" pos="d" xml:id="A79006-001-a-0190">A</w>
      <w lemma="proclamation" pos="n1" xml:id="A79006-001-a-0200">Proclamation</w>
     </hi>
     <w lemma="for" pos="acp" xml:id="A79006-001-a-0210">for</w>
     <w lemma="the" pos="d" xml:id="A79006-001-a-0220">the</w>
     <w lemma="free" pos="j" xml:id="A79006-001-a-0230">free</w>
     <w lemma="and" pos="cc" xml:id="A79006-001-a-0240">and</w>
     <w lemma="safe" pos="j" xml:id="A79006-001-a-0250">safe</w>
     <w lemma="passage" pos="n1" xml:id="A79006-001-a-0260">passage</w>
     <w lemma="of" pos="acp" xml:id="A79006-001-a-0270">of</w>
     <w lemma="all" pos="d" xml:id="A79006-001-a-0280">all</w>
     <hi xml:id="A79006-e150">
      <w lemma="clothes" pos="n2" xml:id="A79006-001-a-0290">Clothes</w>
      <pc xml:id="A79006-001-a-0300">,</pc>
      <w lemma="good" pos="n2-j" xml:id="A79006-001-a-0310">Goods</w>
      <pc xml:id="A79006-001-a-0320">,</pc>
      <w lemma="ware" pos="n2" xml:id="A79006-001-a-0330">Wares</w>
      <pc xml:id="A79006-001-a-0340">,</pc>
     </hi>
     <w lemma="and" pos="cc" xml:id="A79006-001-a-0350">and</w>
     <hi xml:id="A79006-e160">
      <w lemma="merchandise" pos="n1" reg="merchandise" xml:id="A79006-001-a-0360">Merchandize</w>
     </hi>
     <w lemma="to" pos="acp" xml:id="A79006-001-a-0370">to</w>
     <w lemma="our" pos="po" xml:id="A79006-001-a-0380">Our</w>
     <w lemma="city" pos="n1" xml:id="A79006-001-a-0390">City</w>
     <w lemma="of" pos="acp" xml:id="A79006-001-a-0400">of</w>
     <hi xml:id="A79006-e170">
      <w lemma="LONDON" pos="nn1" xml:id="A79006-001-a-0410">LONDON</w>
      <pc unit="sentence" xml:id="A79006-001-a-0420">.</pc>
     </hi>
    </head>
    <p xml:id="A79006-e180">
     <w lemma="whereas" pos="cs" rend="decorinit" xml:id="A79006-001-a-0430">WHereas</w>
     <w lemma="we" pos="pns" xml:id="A79006-001-a-0440">We</w>
     <w lemma="have" pos="vvb" xml:id="A79006-001-a-0450">have</w>
     <w lemma="be" pos="vvn" xml:id="A79006-001-a-0460">been</w>
     <w lemma="inform" pos="vvn" xml:id="A79006-001-a-0470">informed</w>
     <pc xml:id="A79006-001-a-0480">,</pc>
     <w lemma="that" pos="cs" xml:id="A79006-001-a-0490">that</w>
     <w lemma="diverse" pos="j" xml:id="A79006-001-a-0500">diverse</w>
     <w lemma="of" pos="acp" xml:id="A79006-001-a-0510">of</w>
     <w lemma="our" pos="po" xml:id="A79006-001-a-0520">Our</w>
     <w lemma="love" pos="j-vg" xml:id="A79006-001-a-0530">loving</w>
     <w lemma="subject" pos="n2" xml:id="A79006-001-a-0540">Subjects</w>
     <pc xml:id="A79006-001-a-0550">,</pc>
     <w lemma="who" pos="crq" xml:id="A79006-001-a-0560">who</w>
     <w lemma="have" pos="vvb" xml:id="A79006-001-a-0570">have</w>
     <w lemma="be" pos="vvn" xml:id="A79006-001-a-0580">been</w>
     <w lemma="travel" pos="vvg" xml:id="A79006-001-a-0590">travelling</w>
     <w lemma="from" pos="acp" xml:id="A79006-001-a-0600">from</w>
     <w lemma="our" pos="po" xml:id="A79006-001-a-0610">Our</w>
     <w lemma="western" pos="j" reg="Western" xml:id="A79006-001-a-0620">Westerne</w>
     <w lemma="county" pos="n2" xml:id="A79006-001-a-0630">Counties</w>
     <pc xml:id="A79006-001-a-0640">,</pc>
     <w lemma="and" pos="cc" xml:id="A79006-001-a-0650">and</w>
     <w lemma="other" pos="d" xml:id="A79006-001-a-0660">other</w>
     <w lemma="part" pos="n2" xml:id="A79006-001-a-0670">parts</w>
     <w lemma="of" pos="acp" xml:id="A79006-001-a-0680">of</w>
     <w lemma="our" pos="po" xml:id="A79006-001-a-0690">Our</w>
     <w lemma="kingdom" pos="n1" reg="kingdom" xml:id="A79006-001-a-0700">Kingdome</w>
     <w lemma="to" pos="acp" xml:id="A79006-001-a-0710">to</w>
     <w lemma="our" pos="po" xml:id="A79006-001-a-0720">Our</w>
     <w lemma="city" pos="n1" xml:id="A79006-001-a-0730">City</w>
     <w lemma="of" pos="acp" xml:id="A79006-001-a-0740">of</w>
     <hi xml:id="A79006-e190">
      <w lemma="London" pos="nn1" xml:id="A79006-001-a-0750">London</w>
     </hi>
     <w lemma="with" pos="acp" xml:id="A79006-001-a-0760">with</w>
     <w lemma="clothes" pos="n2" xml:id="A79006-001-a-0770">Clothes</w>
     <pc xml:id="A79006-001-a-0780">,</pc>
     <w lemma="good" pos="n2-j" xml:id="A79006-001-a-0790">Goods</w>
     <pc xml:id="A79006-001-a-0800">,</pc>
     <w lemma="and" pos="cc" xml:id="A79006-001-a-0810">and</w>
     <w lemma="other" pos="d" xml:id="A79006-001-a-0820">other</w>
     <w lemma="merchandise" pos="n1" reg="merchandise" xml:id="A79006-001-a-0830">Merchandize</w>
     <pc xml:id="A79006-001-a-0840">,</pc>
     <w lemma="have" pos="vvb" xml:id="A79006-001-a-0850">have</w>
     <w lemma="be" pos="vvn" xml:id="A79006-001-a-0860">been</w>
     <w lemma="of" pos="acp" xml:id="A79006-001-a-0870">of</w>
     <w lemma="late" pos="av-j" xml:id="A79006-001-a-0880">late</w>
     <w lemma="stop" pos="vvn" xml:id="A79006-001-a-0890">stopped</w>
     <w lemma="and" pos="cc" xml:id="A79006-001-a-0900">and</w>
     <w lemma="interrupt" pos="vvn" xml:id="A79006-001-a-0910">interrupted</w>
     <w lemma="in" pos="acp" xml:id="A79006-001-a-0920">in</w>
     <w lemma="their" pos="po" xml:id="A79006-001-a-0930">their</w>
     <w lemma="journey" pos="n2" reg="journeys" xml:id="A79006-001-a-0940">Iournies</w>
     <pc xml:id="A79006-001-a-0950">,</pc>
     <w lemma="and" pos="cc" xml:id="A79006-001-a-0960">and</w>
     <w lemma="other" pos="d" xml:id="A79006-001-a-0970">other</w>
     <w lemma="clothes" pos="n2" xml:id="A79006-001-a-0980">Clothes</w>
     <pc xml:id="A79006-001-a-0990">,</pc>
     <w lemma="ware" pos="n2" xml:id="A79006-001-a-1000">Wares</w>
     <pc xml:id="A79006-001-a-1010">,</pc>
     <w lemma="and" pos="cc" xml:id="A79006-001-a-1020">and</w>
     <w lemma="merchandise" pos="n1" reg="merchandise" xml:id="A79006-001-a-1030">Merchandize</w>
     <w lemma="have" pos="vvb" xml:id="A79006-001-a-1040">have</w>
     <w lemma="be" pos="vvn" xml:id="A79006-001-a-1050">been</w>
     <w lemma="take" pos="vvn" xml:id="A79006-001-a-1060">taken</w>
     <w lemma="or" pos="cc" xml:id="A79006-001-a-1070">or</w>
     <w lemma="detain" pos="vvn" xml:id="A79006-001-a-1080">detained</w>
     <w lemma="from" pos="acp" xml:id="A79006-001-a-1090">from</w>
     <w lemma="they" pos="pno" xml:id="A79006-001-a-1100">them</w>
     <pc xml:id="A79006-001-a-1110">,</pc>
     <w lemma="whereby" pos="crq" xml:id="A79006-001-a-1120">whereby</w>
     <w lemma="the" pos="d" xml:id="A79006-001-a-1130">the</w>
     <w lemma="season" pos="n1" xml:id="A79006-001-a-1140">season</w>
     <w lemma="and" pos="cc" xml:id="A79006-001-a-1150">and</w>
     <w lemma="benefit" pos="n1" xml:id="A79006-001-a-1160">benefit</w>
     <w lemma="of" pos="acp" xml:id="A79006-001-a-1170">of</w>
     <w lemma="their" pos="po" xml:id="A79006-001-a-1180">their</w>
     <w lemma="market" pos="n2" xml:id="A79006-001-a-1190">Markets</w>
     <w lemma="have" pos="vvb" xml:id="A79006-001-a-1200">have</w>
     <w lemma="be" pos="vvn" xml:id="A79006-001-a-1210">been</w>
     <w lemma="lose" pos="vvn" xml:id="A79006-001-a-1220">lost</w>
     <w lemma="to" pos="acp" xml:id="A79006-001-a-1230">to</w>
     <w lemma="they" pos="pno" xml:id="A79006-001-a-1240">them</w>
     <pc xml:id="A79006-001-a-1250">,</pc>
     <w lemma="and" pos="cc" xml:id="A79006-001-a-1260">and</w>
     <w lemma="consider" pos="vvg" xml:id="A79006-001-a-1270">considering</w>
     <pc xml:id="A79006-001-a-1280">,</pc>
     <w lemma="that" pos="cs" xml:id="A79006-001-a-1290">that</w>
     <w lemma="if" pos="cs" xml:id="A79006-001-a-1300">if</w>
     <w lemma="the" pos="d" xml:id="A79006-001-a-1310">the</w>
     <w lemma="same" pos="d" xml:id="A79006-001-a-1320">same</w>
     <w lemma="licence" pos="n1" xml:id="A79006-001-a-1330">Licence</w>
     <w lemma="and" pos="cc" xml:id="A79006-001-a-1340">and</w>
     <w lemma="course" pos="n1" xml:id="A79006-001-a-1350">Course</w>
     <w lemma="shall" pos="vmb" xml:id="A79006-001-a-1360">shall</w>
     <w lemma="be" pos="vvi" xml:id="A79006-001-a-1370">be</w>
     <w lemma="still" pos="av" xml:id="A79006-001-a-1380">still</w>
     <w lemma="take" pos="vvn" xml:id="A79006-001-a-1390">taken</w>
     <w lemma="and" pos="cc" xml:id="A79006-001-a-1400">and</w>
     <w lemma="hold" pos="vvn" xml:id="A79006-001-a-1410">held</w>
     <pc xml:id="A79006-001-a-1420">,</pc>
     <w lemma="that" pos="cs" xml:id="A79006-001-a-1430">that</w>
     <w lemma="the" pos="d" xml:id="A79006-001-a-1440">the</w>
     <w lemma="damage" pos="n1" xml:id="A79006-001-a-1450">damage</w>
     <w lemma="and" pos="cc" xml:id="A79006-001-a-1460">and</w>
     <w lemma="mischief" pos="n1" xml:id="A79006-001-a-1470">mischief</w>
     <w lemma="thereof" pos="av" xml:id="A79006-001-a-1480">thereof</w>
     <w lemma="will" pos="vmb" xml:id="A79006-001-a-1490">will</w>
     <w lemma="not" pos="xx" xml:id="A79006-001-a-1500">not</w>
     <w lemma="only" pos="av-j" xml:id="A79006-001-a-1510">only</w>
     <w lemma="fall" pos="vvi" xml:id="A79006-001-a-1520">fall</w>
     <w lemma="upon" pos="acp" xml:id="A79006-001-a-1530">upon</w>
     <w lemma="place" pos="n2" xml:id="A79006-001-a-1540">Places</w>
     <w lemma="and" pos="cc" xml:id="A79006-001-a-1550">and</w>
     <w lemma="person" pos="n2" xml:id="A79006-001-a-1560">Persons</w>
     <w lemma="disaffect" pos="vvn" xml:id="A79006-001-a-1570">disaffected</w>
     <w lemma="to" pos="acp" xml:id="A79006-001-a-1580">to</w>
     <w lemma="we" pos="pno" reg="us" xml:id="A79006-001-a-1590">Vs</w>
     <pc xml:id="A79006-001-a-1600">,</pc>
     <w lemma="but" pos="acp" xml:id="A79006-001-a-1610">but</w>
     <w lemma="upon" pos="acp" xml:id="A79006-001-a-1620">upon</w>
     <w lemma="very" pos="av" xml:id="A79006-001-a-1630">very</w>
     <w lemma="many" pos="d" xml:id="A79006-001-a-1640">many</w>
     <w lemma="of" pos="acp" xml:id="A79006-001-a-1650">of</w>
     <w lemma="our" pos="po" xml:id="A79006-001-a-1660">Our</w>
     <w lemma="good" pos="j" xml:id="A79006-001-a-1670">good</w>
     <w lemma="and" pos="cc" xml:id="A79006-001-a-1680">and</w>
     <w lemma="love" pos="j-vg" xml:id="A79006-001-a-1690">loving</w>
     <w lemma="subject" pos="n2" xml:id="A79006-001-a-1700">Subjects</w>
     <w lemma="of" pos="acp" xml:id="A79006-001-a-1710">of</w>
     <w lemma="all" pos="d" xml:id="A79006-001-a-1720">all</w>
     <w lemma="part" pos="n2" xml:id="A79006-001-a-1730">parts</w>
     <pc xml:id="A79006-001-a-1740">,</pc>
     <w lemma="and" pos="cc" xml:id="A79006-001-a-1750">and</w>
     <w lemma="that" pos="cs" xml:id="A79006-001-a-1760">that</w>
     <w lemma="thereby" pos="av" xml:id="A79006-001-a-1770">thereby</w>
     <w lemma="the" pos="d" xml:id="A79006-001-a-1780">the</w>
     <w lemma="general" pos="j" reg="general" xml:id="A79006-001-a-1790">generall</w>
     <w lemma="trade" pos="n1" xml:id="A79006-001-a-1800">Trade</w>
     <w lemma="and" pos="cc" xml:id="A79006-001-a-1810">and</w>
     <w lemma="commerce" pos="n1" xml:id="A79006-001-a-1820">Commerce</w>
     <w lemma="of" pos="acp" xml:id="A79006-001-a-1830">of</w>
     <w lemma="the" pos="d" xml:id="A79006-001-a-1840">the</w>
     <w lemma="kingdom" pos="n1" xml:id="A79006-001-a-1850">Kingdom</w>
     <pc join="right" xml:id="A79006-001-a-1860">(</pc>
     <w lemma="which" pos="crq" xml:id="A79006-001-a-1870">which</w>
     <w lemma="we" pos="pns" xml:id="A79006-001-a-1880">We</w>
     <w lemma="have" pos="vvb" xml:id="A79006-001-a-1890">have</w>
     <w lemma="always" pos="av" reg="always" xml:id="A79006-001-a-1900">alwayes</w>
     <pc xml:id="A79006-001-a-1910">,</pc>
     <w lemma="and" pos="cc" xml:id="A79006-001-a-1920">and</w>
     <w lemma="do" pos="vvb" xml:id="A79006-001-a-1930">do</w>
     <w lemma="desire" pos="n1" xml:id="A79006-001-a-1940">desire</w>
     <w lemma="to" pos="prt" xml:id="A79006-001-a-1950">to</w>
     <w lemma="advance" pos="vvi" xml:id="A79006-001-a-1960">advance</w>
     <w lemma="to" pos="acp" xml:id="A79006-001-a-1970">to</w>
     <w lemma="the" pos="d" xml:id="A79006-001-a-1980">the</w>
     <w lemma="utmost" pos="j" xml:id="A79006-001-a-1990">utmost</w>
     <w lemma="of" pos="acp" xml:id="A79006-001-a-2000">of</w>
     <w lemma="our" pos="po" xml:id="A79006-001-a-2010">Our</w>
     <w lemma="power" pos="n1" xml:id="A79006-001-a-2020">Power</w>
     <pc xml:id="A79006-001-a-2030">)</pc>
     <w lemma="will" pos="vmb" xml:id="A79006-001-a-2040">will</w>
     <w lemma="in" pos="acp" xml:id="A79006-001-a-2050">in</w>
     <w lemma="a" pos="d" xml:id="A79006-001-a-2060">a</w>
     <w lemma="short" pos="j" xml:id="A79006-001-a-2070">short</w>
     <w lemma="time" pos="n1" xml:id="A79006-001-a-2080">time</w>
     <w lemma="decay" pos="n1" xml:id="A79006-001-a-2090">decay</w>
     <pc xml:id="A79006-001-a-2100">,</pc>
     <w lemma="and" pos="cc" xml:id="A79006-001-a-2110">and</w>
     <w lemma="the" pos="d" xml:id="A79006-001-a-2120">the</w>
     <w lemma="poor" pos="j" reg="poor" xml:id="A79006-001-a-2130">poore</w>
     <w lemma="people" pos="n1" xml:id="A79006-001-a-2140">People</w>
     <pc xml:id="A79006-001-a-2150">,</pc>
     <w lemma="want" pos="vvg" xml:id="A79006-001-a-2160">wanting</w>
     <w lemma="work" pos="n1" xml:id="A79006-001-a-2170">work</w>
     <pc xml:id="A79006-001-a-2180">,</pc>
     <w lemma="be" pos="vvb" xml:id="A79006-001-a-2190">be</w>
     <w lemma="bring" pos="vvn" xml:id="A79006-001-a-2200">brought</w>
     <w lemma="to" pos="acp" xml:id="A79006-001-a-2210">to</w>
     <w lemma="penury" pos="n1" xml:id="A79006-001-a-2220">Penury</w>
     <w lemma="and" pos="cc" xml:id="A79006-001-a-2230">and</w>
     <w lemma="famine" pos="n1" xml:id="A79006-001-a-2240">Famine</w>
     <pc unit="sentence" xml:id="A79006-001-a-2250">.</pc>
     <w lemma="we" pos="pns" reg="We" xml:id="A79006-001-a-2260">Wee</w>
     <w lemma="be" pos="vvb" xml:id="A79006-001-a-2270">are</w>
     <w lemma="gracious" pos="av-j" reg="graciously" xml:id="A79006-001-a-2280">gratiously</w>
     <w lemma="please" pos="vvn" xml:id="A79006-001-a-2290">pleased</w>
     <w lemma="to" pos="prt" xml:id="A79006-001-a-2300">to</w>
     <w lemma="declare" pos="vvi" xml:id="A79006-001-a-2310">declare</w>
     <pc xml:id="A79006-001-a-2320">,</pc>
     <w lemma="and" pos="cc" xml:id="A79006-001-a-2330">and</w>
     <w lemma="do" pos="vvb" reg="do" xml:id="A79006-001-a-2340">doe</w>
     <w lemma="hereby" pos="av" xml:id="A79006-001-a-2350">hereby</w>
     <w lemma="will" pos="vmb" xml:id="A79006-001-a-2360">will</w>
     <w lemma="and" pos="cc" xml:id="A79006-001-a-2370">and</w>
     <w lemma="require" pos="vvi" xml:id="A79006-001-a-2380">require</w>
     <w lemma="all" pos="d" xml:id="A79006-001-a-2390">all</w>
     <w lemma="the" pos="d" xml:id="A79006-001-a-2400">the</w>
     <w lemma="officer" pos="n2" xml:id="A79006-001-a-2410">Officers</w>
     <w lemma="and" pos="cc" xml:id="A79006-001-a-2420">and</w>
     <w lemma="soldier" pos="n2" reg="soldiers" xml:id="A79006-001-a-2430">Souldiers</w>
     <w lemma="of" pos="acp" xml:id="A79006-001-a-2440">of</w>
     <w lemma="our" pos="po" xml:id="A79006-001-a-2450">Our</w>
     <w lemma="army" pos="n1" xml:id="A79006-001-a-2460">Army</w>
     <pc xml:id="A79006-001-a-2470">,</pc>
     <w lemma="and" pos="cc" xml:id="A79006-001-a-2480">and</w>
     <w lemma="all" pos="d" xml:id="A79006-001-a-2490">all</w>
     <w lemma="other" pos="d" xml:id="A79006-001-a-2500">other</w>
     <w lemma="our" pos="po" xml:id="A79006-001-a-2510">Our</w>
     <w lemma="officer" pos="n2" xml:id="A79006-001-a-2520">Officers</w>
     <w lemma="and" pos="cc" xml:id="A79006-001-a-2530">and</w>
     <w lemma="minister" pos="n2" xml:id="A79006-001-a-2540">Ministers</w>
     <w lemma="whatsoever" pos="crq" xml:id="A79006-001-a-2550">whatsoever</w>
     <pc xml:id="A79006-001-a-2560">,</pc>
     <w lemma="that" pos="cs" xml:id="A79006-001-a-2570">that</w>
     <w lemma="from" pos="acp" xml:id="A79006-001-a-2580">from</w>
     <w lemma="henceforth" pos="av" xml:id="A79006-001-a-2590">henceforth</w>
     <w lemma="they" pos="pns" xml:id="A79006-001-a-2600">they</w>
     <w lemma="give" pos="vvb" reg="give" xml:id="A79006-001-a-2610">giue</w>
     <w lemma="no" pos="dx" xml:id="A79006-001-a-2620">no</w>
     <w lemma="stop" pos="n1" xml:id="A79006-001-a-2630">stop</w>
     <w lemma="or" pos="cc" xml:id="A79006-001-a-2640">or</w>
     <w lemma="interruption" pos="n1" xml:id="A79006-001-a-2650">interruption</w>
     <w lemma="to" pos="acp" xml:id="A79006-001-a-2660">to</w>
     <w lemma="any" pos="d" xml:id="A79006-001-a-2670">any</w>
     <w lemma="of" pos="acp" xml:id="A79006-001-a-2680">of</w>
     <w lemma="our" pos="po" xml:id="A79006-001-a-2690">Our</w>
     <w lemma="love" pos="j-vg" xml:id="A79006-001-a-2700">loving</w>
     <w lemma="subject" pos="n2" xml:id="A79006-001-a-2710">Subjects</w>
     <w lemma="as" pos="acp" xml:id="A79006-001-a-2720">as</w>
     <w lemma="they" pos="pns" xml:id="A79006-001-a-2730">they</w>
     <w lemma="travel" pos="vvb" reg="travel" xml:id="A79006-001-a-2740">travell</w>
     <w lemma="to" pos="acp" xml:id="A79006-001-a-2750">to</w>
     <w lemma="our" pos="po" xml:id="A79006-001-a-2760">Our</w>
     <w lemma="city" pos="n1" xml:id="A79006-001-a-2770">City</w>
     <w lemma="of" pos="acp" xml:id="A79006-001-a-2780">of</w>
     <hi xml:id="A79006-e200">
      <w lemma="London" pos="nn1" xml:id="A79006-001-a-2790">London</w>
     </hi>
     <w lemma="with" pos="acp" xml:id="A79006-001-a-2800">with</w>
     <w lemma="any" pos="d" xml:id="A79006-001-a-2810">any</w>
     <w lemma="clothes" pos="n2" xml:id="A79006-001-a-2820">Clothes</w>
     <pc xml:id="A79006-001-a-2830">,</pc>
     <w lemma="ware" pos="n2" xml:id="A79006-001-a-2840">Wares</w>
     <pc xml:id="A79006-001-a-2850">,</pc>
     <w lemma="or" pos="cc" xml:id="A79006-001-a-2860">or</w>
     <w lemma="other" pos="d" xml:id="A79006-001-a-2870">other</w>
     <w lemma="merchandise" pos="n1" reg="merchandise" xml:id="A79006-001-a-2880">Merchandize</w>
     <pc xml:id="A79006-001-a-2890">,</pc>
     <w lemma="but" pos="acp" xml:id="A79006-001-a-2900">but</w>
     <w lemma="that" pos="cs" xml:id="A79006-001-a-2910">that</w>
     <w lemma="they" pos="pns" xml:id="A79006-001-a-2920">they</w>
     <w lemma="suffer" pos="vvb" xml:id="A79006-001-a-2930">suffer</w>
     <w lemma="they" pos="pno" xml:id="A79006-001-a-2940">them</w>
     <pc xml:id="A79006-001-a-2950">,</pc>
     <w lemma="and" pos="cc" xml:id="A79006-001-a-2960">and</w>
     <w lemma="such" pos="d" xml:id="A79006-001-a-2970">such</w>
     <w lemma="their" pos="po" xml:id="A79006-001-a-2980">their</w>
     <w lemma="clothes" pos="n2" xml:id="A79006-001-a-2990">Clothes</w>
     <pc xml:id="A79006-001-a-3000">,</pc>
     <w lemma="ware" pos="n2" xml:id="A79006-001-a-3010">Wares</w>
     <pc xml:id="A79006-001-a-3020">,</pc>
     <w lemma="and" pos="cc" xml:id="A79006-001-a-3030">and</w>
     <w lemma="merchandise" pos="n1" reg="merchandise" xml:id="A79006-001-a-3040">Merchandize</w>
     <w lemma="free" pos="av-j" xml:id="A79006-001-a-3050">freely</w>
     <w lemma="and" pos="cc" xml:id="A79006-001-a-3060">and</w>
     <w lemma="peaceable" pos="av-j" xml:id="A79006-001-a-3070">peaceably</w>
     <w lemma="to" pos="prt" xml:id="A79006-001-a-3080">to</w>
     <w lemma="pass" pos="vvi" reg="pass" xml:id="A79006-001-a-3090">passe</w>
     <w lemma="without" pos="acp" xml:id="A79006-001-a-3100">without</w>
     <w lemma="any" pos="d" xml:id="A79006-001-a-3110">any</w>
     <w lemma="let" pos="vvb" xml:id="A79006-001-a-3120">let</w>
     <pc xml:id="A79006-001-a-3130">,</pc>
     <w lemma="trouble" pos="n1" xml:id="A79006-001-a-3140">trouble</w>
     <pc xml:id="A79006-001-a-3150">,</pc>
     <w lemma="or" pos="cc" xml:id="A79006-001-a-3160">or</w>
     <w lemma="molestation" pos="n1" xml:id="A79006-001-a-3170">molestation</w>
     <w lemma="whatsoever" pos="crq" xml:id="A79006-001-a-3180">whatsoever</w>
     <pc unit="sentence" xml:id="A79006-001-a-3190">.</pc>
     <w lemma="and" pos="cc" xml:id="A79006-001-a-3200">And</w>
     <w lemma="we" pos="pns" xml:id="A79006-001-a-3210">We</w>
     <w lemma="do" pos="vvb" reg="do" xml:id="A79006-001-a-3220">doe</w>
     <w lemma="hereby" pos="av" xml:id="A79006-001-a-3230">hereby</w>
     <w lemma="promise" pos="vvi" xml:id="A79006-001-a-3240">promise</w>
     <w lemma="and" pos="cc" xml:id="A79006-001-a-3250">and</w>
     <w lemma="assure" pos="vvi" xml:id="A79006-001-a-3260">assure</w>
     <w lemma="all" pos="d" xml:id="A79006-001-a-3270">all</w>
     <w lemma="our" pos="po" xml:id="A79006-001-a-3280">Our</w>
     <w lemma="love" pos="j-vg" xml:id="A79006-001-a-3290">loving</w>
     <w lemma="subject" pos="n2" xml:id="A79006-001-a-3300">Subjects</w>
     <pc xml:id="A79006-001-a-3310">,</pc>
     <w lemma="that" pos="cs" xml:id="A79006-001-a-3320">that</w>
     <w lemma="if" pos="cs" xml:id="A79006-001-a-3330">if</w>
     <w lemma="they" pos="pns" xml:id="A79006-001-a-3340">they</w>
     <w lemma="shall" pos="vmb" xml:id="A79006-001-a-3350">shall</w>
     <w lemma="henceforth" pos="av" xml:id="A79006-001-a-3360">henceforth</w>
     <w lemma="suffer" pos="vvi" xml:id="A79006-001-a-3370">suffer</w>
     <w lemma="by" pos="acp" xml:id="A79006-001-a-3380">by</w>
     <w lemma="any" pos="d" xml:id="A79006-001-a-3390">any</w>
     <w lemma="soldier" pos="n2" reg="soldiers" xml:id="A79006-001-a-3400">Souldiers</w>
     <w lemma="of" pos="acp" xml:id="A79006-001-a-3410">of</w>
     <w lemma="our" pos="po" xml:id="A79006-001-a-3420">Our</w>
     <w lemma="army" pos="n1" xml:id="A79006-001-a-3430">Army</w>
     <w lemma="in" pos="acp" xml:id="A79006-001-a-3440">in</w>
     <w lemma="this" pos="d" xml:id="A79006-001-a-3450">this</w>
     <w lemma="case" pos="n1" xml:id="A79006-001-a-3460">Case</w>
     <pc xml:id="A79006-001-a-3470">,</pc>
     <w lemma="and" pos="cc" xml:id="A79006-001-a-3480">and</w>
     <w lemma="shall" pos="vmb" xml:id="A79006-001-a-3490">shall</w>
     <w lemma="not" pos="xx" xml:id="A79006-001-a-3500">not</w>
     <w lemma="upon" pos="acp" xml:id="A79006-001-a-3510">upon</w>
     <w lemma="complaint" pos="n1" xml:id="A79006-001-a-3520">Complaint</w>
     <w lemma="to" pos="acp" xml:id="A79006-001-a-3530">to</w>
     <w lemma="the" pos="d" xml:id="A79006-001-a-3540">the</w>
     <w lemma="chief" pos="j" xml:id="A79006-001-a-3550">chief</w>
     <w lemma="officer" pos="n2" xml:id="A79006-001-a-3560">Officers</w>
     <w lemma="of" pos="acp" xml:id="A79006-001-a-3570">of</w>
     <w lemma="our" pos="po" xml:id="A79006-001-a-3580">Our</w>
     <w lemma="army" pos="n1" xml:id="A79006-001-a-3590">Army</w>
     <w lemma="where" pos="crq" xml:id="A79006-001-a-3600">where</w>
     <w lemma="such" pos="d" xml:id="A79006-001-a-3610">such</w>
     <w lemma="damage" pos="n1" xml:id="A79006-001-a-3620">damage</w>
     <w lemma="be" pos="vvz" xml:id="A79006-001-a-3630">is</w>
     <w lemma="suffer" pos="vvn" xml:id="A79006-001-a-3640">suffered</w>
     <pc xml:id="A79006-001-a-3650">,</pc>
     <w lemma="receive" pos="vvb" xml:id="A79006-001-a-3660">receive</w>
     <w lemma="justice" pos="n1" reg="justice" xml:id="A79006-001-a-3670">Iustice</w>
     <w lemma="and" pos="cc" xml:id="A79006-001-a-3680">and</w>
     <w lemma="reparation" pos="n1" xml:id="A79006-001-a-3690">Reparation</w>
     <w lemma="for" pos="acp" xml:id="A79006-001-a-3700">for</w>
     <w lemma="the" pos="d" xml:id="A79006-001-a-3710">the</w>
     <w lemma="damage" pos="n1" xml:id="A79006-001-a-3720">damage</w>
     <w lemma="they" pos="pns" xml:id="A79006-001-a-3730">they</w>
     <w lemma="sustain" pos="vvb" reg="sustain" xml:id="A79006-001-a-3740">sustaine</w>
     <pc xml:id="A79006-001-a-3750">,</pc>
     <w lemma="upon" pos="acp" xml:id="A79006-001-a-3760">upon</w>
     <w lemma="complaint" pos="n1" xml:id="A79006-001-a-3770">complaint</w>
     <w lemma="make" pos="vvn" xml:id="A79006-001-a-3780">made</w>
     <w lemma="to" pos="acp" xml:id="A79006-001-a-3790">to</w>
     <w lemma="we" pos="pno" reg="us" xml:id="A79006-001-a-3800">Vs</w>
     <w lemma="we" pos="pns" xml:id="A79006-001-a-3810">We</w>
     <w lemma="will" pos="vmb" xml:id="A79006-001-a-3820">will</w>
     <w lemma="take" pos="vvi" xml:id="A79006-001-a-3830">take</w>
     <w lemma="speedy" pos="j" xml:id="A79006-001-a-3840">speedy</w>
     <w lemma="care" pos="n1" xml:id="A79006-001-a-3850">care</w>
     <w lemma="for" pos="acp" xml:id="A79006-001-a-3860">for</w>
     <w lemma="the" pos="d" xml:id="A79006-001-a-3870">the</w>
     <w lemma="severe" pos="j" xml:id="A79006-001-a-3880">severe</w>
     <w lemma="and" pos="cc" xml:id="A79006-001-a-3890">and</w>
     <w lemma="exemplary" pos="j" xml:id="A79006-001-a-3900">exemplary</w>
     <w lemma="punishment" pos="n1" xml:id="A79006-001-a-3910">punishment</w>
     <w lemma="of" pos="acp" xml:id="A79006-001-a-3920">of</w>
     <w lemma="the" pos="d" xml:id="A79006-001-a-3930">the</w>
     <w lemma="offender" pos="n2" reg="offenders" xml:id="A79006-001-a-3940">Offendors</w>
     <pc xml:id="A79006-001-a-3950">,</pc>
     <w lemma="and" pos="cc" xml:id="A79006-001-a-3960">and</w>
     <w lemma="for" pos="acp" xml:id="A79006-001-a-3970">for</w>
     <w lemma="the" pos="d" xml:id="A79006-001-a-3980">the</w>
     <w lemma="full" pos="j" xml:id="A79006-001-a-3990">full</w>
     <w lemma="satisfaction" pos="n1" xml:id="A79006-001-a-4000">satisfaction</w>
     <w lemma="of" pos="acp" xml:id="A79006-001-a-4010">of</w>
     <w lemma="the" pos="d" xml:id="A79006-001-a-4020">the</w>
     <w lemma="party" pos="n2" xml:id="A79006-001-a-4030">Parties</w>
     <w lemma="grieve" pos="vvn" xml:id="A79006-001-a-4040">grieved</w>
     <w lemma="and" pos="cc" xml:id="A79006-001-a-4050">and</w>
     <w lemma="injure" pos="vvn" xml:id="A79006-001-a-4060">injured</w>
     <pc unit="sentence" xml:id="A79006-001-a-4070">.</pc>
    </p>
    <closer xml:id="A79006-e210">
     <dateline xml:id="A79006-e220">
      <w lemma="give" pos="vvn" xml:id="A79006-001-a-4080">Given</w>
      <w lemma="at" pos="acp" xml:id="A79006-001-a-4090">at</w>
      <w lemma="our" pos="po" xml:id="A79006-001-a-4100">Our</w>
      <w lemma="court" pos="n1" xml:id="A79006-001-a-4110">Court</w>
      <w lemma="at" pos="acp" xml:id="A79006-001-a-4120">at</w>
      <hi xml:id="A79006-e230">
       <w lemma="Oxford" pos="nn1" xml:id="A79006-001-a-4130">Oxford</w>
       <pc xml:id="A79006-001-a-4140">,</pc>
      </hi>
      <date xml:id="A79006-e240">
       <w lemma="the" pos="d" xml:id="A79006-001-a-4150">the</w>
       <w lemma="eight" pos="crd" xml:id="A79006-001-a-4160">eight</w>
       <w lemma="day" pos="n1" xml:id="A79006-001-a-4170">day</w>
       <w lemma="of" pos="acp" xml:id="A79006-001-a-4180">of</w>
       <hi xml:id="A79006-e250">
        <w lemma="December" pos="nn1" xml:id="A79006-001-a-4190">December</w>
        <pc xml:id="A79006-001-a-4200">,</pc>
       </hi>
       <w lemma="in" pos="acp" xml:id="A79006-001-a-4210">in</w>
       <w lemma="the" pos="d" xml:id="A79006-001-a-4220">the</w>
       <w lemma="eighteen" pos="ord" xml:id="A79006-001-a-4230">Eighteenth</w>
       <w lemma="year" pos="n1" reg="year" xml:id="A79006-001-a-4240">yeare</w>
       <w lemma="of" pos="acp" xml:id="A79006-001-a-4250">of</w>
       <w lemma="our" pos="po" xml:id="A79006-001-a-4260">Our</w>
       <w lemma="reign" pos="n1" reg="reign" xml:id="A79006-001-a-4270">Reigne</w>
       <pc unit="sentence" xml:id="A79006-001-a-4280">.</pc>
      </date>
     </dateline>
    </closer>
    <closer xml:id="A79006-e260">
     <w lemma="God" pos="nn1" xml:id="A79006-001-a-4290">God</w>
     <w lemma="save" pos="acp" xml:id="A79006-001-a-4300">save</w>
     <w lemma="the" pos="d" xml:id="A79006-001-a-4310">the</w>
     <w lemma="king" pos="n1" xml:id="A79006-001-a-4320">King</w>
     <pc unit="sentence" xml:id="A79006-001-a-4330">.</pc>
    </closer>
   </div>
  </body>
 </text>
</TEI>
